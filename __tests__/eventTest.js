import React from 'react';
import renderer from 'react-test-renderer';
import EventsScreen from '../src/components/Events';


test('renders correctly', () => {
  const tree = renderer.create(<EventsScreen />).toJSON();
  expect(tree).toMatchSnapshot();
});


