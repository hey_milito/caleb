import React from 'react';
import  { Component } from 'react';
import { shallow,configure } from 'enzyme';
import DetailsScreen from '../src/components/EventDetails';
import Adapter from 'enzyme-adapter-react-16'

configure({ adapter: new Adapter() })

describe('Event Details Screen', () => {

    it('should render Event Details component', () => {
        <DetailsScreen navigation={{ getParam: jest.fn() }} />
    });
  
    it('should render initial layout', () => {
    // when
    const component = shallow(<DetailsScreen navigation={{ getParam: jest.fn() }} />);
    // then
    expect(component.getElements()).toMatchSnapshot();
    });
  
    it('should check if BackButton exists', () => {
      const wrapper = shallow(<DetailsScreen navigation={{ getParam: jest.fn() }} />);
      wrapper.findWhere(n => n.name() === 'Button' && n.prop('testID') === 'HomeButton')
  
    });
  
  });
