import App from './../App';
import { shallow, ShallowWrapper, configure } from "enzyme";
import React from "react";
import { View } from "react-native";
import "../jest.setup"
import Adapter from 'enzyme-adapter-react-16'
import renderer from 'react-test-renderer';


configure({ adapter: new Adapter() })
const createTestProps = (props: Object) => ({
  navigation: {
    navigate: jest.fn()

  },
  ...props
});

describe("app naviagator", () => {
  describe("rendering", () => {
    let wrapper: ShallowWrapper;
    let props: any;   // use type "any" to opt-out of type-checking
    beforeEach(() => {
      props = createTestProps({});
      wrapper = shallow(<App {...props} />);   // no compile-time error
    });

    it(`has two two children`, () => {
      const tree = renderer.create(<App />).toJSON();
      expect(tree.children.length)==2;
    });

    
  });
});