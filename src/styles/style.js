import { StyleSheet, Platform } from 'react-native';

export default StyleSheet.create({
    MainContainer :{
          
        justifyContent: 'center',
        flex:1,
        margin: 5,
        marginTop: (Platform.OS === 'ios') ? 20 : 0,
     
    },
    itemSeperator: {
        height: .5,
        width: "100%",
        backgroundColor: "#000",
    },
     
    imageView: {
     
        width: '50%',
        height: 100 ,
        margin: 7,
        borderRadius : 7
    },
     
    textView: {
     
        width:'50%', 
        textAlignVertical:'center',
        padding:10,
        color: '#000'
     
    },
    startdatetext: {
     
       width:'100%', 
       textAlignVertical:'center',
       padding:10,
       color: '#00A36C'
    
   },
   MainContainerDetails :{
     
    justifyContent: 'center',
    flex:1,
    margin: 5,
    marginTop: (Platform.OS === 'ios') ? 20 : 0,
 
},
webView: {
    backgroundColor: 'rgba(255,255,255,0.8)',
    height: 100,
  },
 
  button: {
    color:"#009933" 
    
  },
});