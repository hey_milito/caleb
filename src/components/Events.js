import React, { Component } from 'react';
import { Platform, View, ActivityIndicator, FlatList, Text, Image, Alert,Pressable, RefreshControl } from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import moment from 'moment';
import checkConnection from '../networkUtils/checkConnection';
import styling from '../styles/style'

export default class EventsScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
          isLoading: true,
          isFetching: false
        }
      }


//Method to redirect to detail screen when event is clicked and passing arguments to the view
      GetItem (eventName, html, link) {
        this.props.navigation.navigate('Details', {  
            eventName: eventName,  
            webview: html,  
            eventLink: link
        })
        
        }

      
//line demacation for the flatlist.      
        FlatListItemSeparator = () => {
          return (
            <View
              style={styling.itemSeperator}
            />
          );
        }
//Method to retrieve data from the API
//I thought about having a different file to make API calls but this demo requires only one network call so it seemed like an overreach.
        webCall=async () =>{
            const isConnected = await checkConnection.isNetworkAvailable()
            if (isConnected === false) {
                //if not connected, trigger alert dialog
                this.networkAlertFunction()
                
              }
               else {
                return fetch('https://thedistance.co.uk/wp-content/uploads/2020/01/eventbrite.json')
                   .then((response) => response.json())
                   .then((responseJson) => {
                    console.log(responseJson)
                    console.log('refreshed data');
                     this.setState({
                       isLoading: false,
                       isFetching: false,
                       dataSource: responseJson
                     }, function() {
                       // In this block you can do something with new state.
                     });
                   })
                   .catch((error) => {
                     console.error(error);
                     this.networkAlertFunction()
                   });
              }
           }


           //Retry network check
           networkAlertFunction = () => {
            //alert can't be cancelled until the user is internet connected and tries again to make network call
            Alert.alert(
               
              'Oops',
              'Looks like you are not connected to the internet or there is an issue retrieving data, check internet connection and retry',
              [
                {text: 'Try again', onPress: () => this.webCall()},
              ],
              { cancelable: false }
              
            );
          }

           
        //method to refresh flatlist with RefreshControl
          onRefresh() {
            this.setState({
               isFetching: true,
            }, () => {
               this.webCall();
            });
         }
           
//Automatically starting the API call if component is mounted       
        componentDidMount(){
           
            this.webCall();
           
           }
           
           render() {
            // show indicator while loading
            if (this.state.isLoading) {
              return (
          
               <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          
                  <ActivityIndicator size="large" />
          
                </View>
                
              );
          
            }
            
          
            return (
          
              <View style={styling.MainContainer}>
          
                <FlatList
                
                 data={ this.state.dataSource.events }
                 
                 ItemSeparatorComponent = {this.FlatListItemSeparator}
          
                 renderItem={({item}) => 
                 
                     <View style={{flex:1}}>
                         <Pressable onPress={this.GetItem.bind(this,item.name.text, item.description.html, item.url)}>
  

                         <View style={{flexDirection: 'row'}}>
                         <Image source = {{ uri: item.logo.url}} style={styling.imageView} />
                      <Text  style={styling.textView} >{item.name.text}</Text> 
                        </View>
                      
                      <View>
                    {/*Date is formatted to a nicer format using moment lirary*/}  
                      <Text style={styling.startdatetext} >{moment.utc(item.start.utc).local().format('llll')}</Text>
                        </View>
                        </Pressable>
                     </View>
           
               
                   }
          
                 keyExtractor={(item, index) => index.toString()}
                 //setting refresh control to refresh view(flatlist)*
                 refreshControl={
                    <RefreshControl
                    colors={["#9Bd35A", "#689F38"]}
                    enabled={true}
                    onRefresh={() => this.onRefresh()}
                    refreshing={this.state.isFetching}
                    />
                  }
                 />
          
              </View>
            );
          }
         }
          

     