import React, { Component } from 'react';
import { Platform, Button, View, Text ,Alert, Linking} from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import { WebView } from 'react-native-webview';
import styling from '../styles/style'





export default class DetailsScreen extends Component {

    //function to open external link on device with linking to make booking
    onPressLearnMore (eventLink)  {
        console.log('external link clicked');
        Linking.openURL(eventLink).catch((err) => console.error('An error occurred', err));
      }
   

  render() {
      {/*Retrieving the needed data for detail view from previous screen*/}  
      const { navigation } = this.props; 
      const eventName = navigation.getParam('eventName', 'no event');  
      const webview = navigation.getParam('webview', 'no html');  
      const eventLink = navigation.getParam('eventLink', 'no link');
    return (
    
        <View style={styling.MainContainerDetails}>
        {/* Thought about including an ActivityIndicator in the webview but since the data is already avaialable from the previous screen and not loaded dynamically from the internet,
        It was not necessary since there is no waiting time to retrieve the data and load*/}  
        <WebView
          originWhitelist={['*']}
          source={{ html: JSON.stringify(webview) }}
          style={styling.webView}
        />
        {/*Button to open the link externall for more detail/booking on eventbrite*/}  
        <Button  
        onPress={this.onPressLearnMore.bind(this,eventLink)}  
        title={eventName+ " Booking"}
        style={styling.button}
                    />  
      </View>
    )
  }


}

