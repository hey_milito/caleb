/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type {Node} from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {createAppContainer } from "react-navigation";
import { createStackNavigator } from 'react-navigation-stack';
//importing the screens
import EventsScreen from './src/components/Events';
import DetailScreen from './src/components/EventDetails';


export default class App extends React.Component {
  render() {
    return <AppContainer />;
  }
}
//Screen routes
const AppNavigator = createStackNavigator({
  Event: {
    screen: EventsScreen
    
  },
  Details: {
    screen: DetailScreen
  }
},{
        initialRouteName: "Event"
});

const AppContainer = createAppContainer(AppNavigator);




